# `SPRINT-5`

- [Save Management](#save-management)
- [Diary Management](#diary-management)
- [Renew UI](#renew-ui)
- [Other Request](#other-request)
- [User Trainer](#user-trainer) (sprint-6)
- [Seller Trainer](#seller-trainer) (sprint-6)
- [Task BE](#task-be)

## `Save Management`

- <new>[APP-USER] FAVORITE - List Favorite Seller (Folder Default)</new>
  ![Alt text](<Screenshot 2023-07-18 at 09.01.34-1.png>)
- <new>[APP-USER] FAVORITE - Save/UnSave Favorite Post/Service/Store/Location </new>
  ![Alt text](<Screenshot 2023-07-18 at 09.03.55.png>)
- <new>[APP-USER] FAVORITE - Add/Create/Update/List Folder Favorite</new>
  ![Alt text](<Screenshot 2023-07-18 at 09.09.48.png>)
- <new>[APP-USER] FAVORITE - Move/Delete Folder Favorite</new>
  ![Alt text](<Screenshot 2023-07-18 at 09.09.04.png>)
  ![Alt text](<Screenshot 2023-07-18 at 09.10.08.png>)

## `Renew UI`

- <new>[APP-USER] RENEW UI - Home</new>
  ![Alt text](<Screenshot 2023-07-18 at 12.39.26.png>)

- <new>[APP-USER] RENEW UI - My Page</new>
  ![Alt text](<Screenshot 2023-07-18 at 12.39.43.png>)
  ![Alt text](<Screenshot 2023-07-18 at 12.39.57.png>)

- <new>[APP-USER] RENEW UI - Profile</new>
  ![Alt text](<Screenshot 2023-07-18 at 12.40.10.png>)
- <new>[APP-USER] RENEW UI - Pet</new>
  ![Alt text](<Screenshot 2023-07-18 at 12.40.21.png>)

- <new>[APP-USER] RENEW UI - Review</new>
  ![Alt text](<Screenshot 2023-07-18 at 12.42.59.png>)
  ![Alt text](<Screenshot 2023-07-18 at 12.43.23.png>)

- <new>[APP-USER] RENEW UI - CARD LIST</new>
  ![Alt text](<Screenshot 2023-07-18 at 21.29.14.png>)

￼- <new>[APP-USER] RENEW UI - CHAT LIST</new>
￼![Alt text](<Screenshot 2023-07-19 at 09.04.01.png>)
￼![Alt text](<Screenshot 2023-07-19 at 09.05.44.png>)
￼
￼￼
￼

## `Diary Management`

- <new>[APP-SELLER] DIARY - List Diary</new>
  ![Alt text](<Screenshot 2023-07-18 at 21.33.32.png>)
- <new>[APP-SELLER] DIARY - Update Diary</new>
  ![Alt text](<Screenshot 2023-07-18 at 21.33.55-1.png>)
  ![Alt text](<Screenshot 2023-07-18 at 21.35.55.png>)
- <new>[APP-SELLER] DIARY - Create Diary care diary</new>
  ![Alt text](<Screenshot 2023-07-18 at 21.33.55-1.png>)
  ![Alt text](<Screenshot 2023-07-18 at 21.35.55.png>)
- <new>[APP-SELLER] DIARY - Create update status</new>
  ![Alt text](<Screenshot 2023-07-18 at 21.33.55-1.png>)
  ![Alt text](<Screenshot 2023-07-18 at 21.35.55.png>)

## `Other Request`

[`Table detail`](./Table.md)

- <new>[APP-SELLER] MY_PAGE - SETTING NOTIFICATION</new>
  ![Alt text](<Screenshot 2023-07-18 at 21.32.26.png>)
- <new>[APP-SELLER]NOTIFICATION - List</new>
  ![Alt text](<Screenshot 2023-07-18 at 21.33.06.png>)
- <new>[APP-USER] REVIEW - Delete review</new>
  ![Alt text](<Screenshot 2023-07-18 at 21.36.47.png>)
- <new>[APP] Fix bugs for 1st release</new>
- <new>[APP-SELLER] SETTLEMENT - Update account</new>
- <new>[APP-SELLER] BANNER - List</new>
- <new>[APP-SELLER] CHAT - Chat with trainer</new>
- <new>[APP-SELLER] CHAT - Chat bot</new>

## `User Trainer`

- <new>[APP-USER] TRAINER - LIST/FILTER</new>
  ![Alt text](<Screenshot 2023-07-18 at 21.28.27.png>)
- <new>[APP-USER] TRAINER - DETAIL</new>
  ![Alt text](<Screenshot 2023-07-18 at 21.28.45.png>)

## `Seller Trainer`

## `Task BE`

```jsx
_## Save Management Sprint-5
BE API User App: Save Management > Folder > Create
BE API User App: Save Management > Folder > Edit
BE API User App: Save Management > Folder > List
BE API User App: Save Management > Folder > Delete
BE API User App: Save Management > Save > Move To Another Folder
BE API User App: Save Management > Save: Post/Service/Store/Location
BE API User App: Save > is Saved: Service/Post/Store/Location in list/detail
BE API User App: Save Management > Save > Folder > List
BE API User App: Save Management > Unsave


_## Diary Management Sprint-6
[APP-SELLER]  DIARY - List Diary


_## Service Trainer Sprint-5
BE API Seller App: Service Trainer - Create
BE API Seller App: Service Trainer - Update
BE API Seller App: Service Trainer - List
BE API Seller App: Service Trainer - Update Status

BE API User App: Service Trainer - List
BE API User App: Service Trainer - Detail


_## Booking Trainer Sprint-5
BE API User App: Service Trainer - Booking - Check Available Time & Overlap Time
BE API User App: Service Trainer - Booking - Calculate Cost
BE API User App: Service Trainer - Booking
BE API User App: Service Trainer - Booking - Cancel

BE API Seller App: Service Trainer - Booking - List / filter
BE API Seller App: Pressed request payment button - Popup info
BE API Seller App: Service Trainer - Booking - List/ Filter
BE API Seller App: Service Trainer - Booking - Confirm / Reject

```

<style>
pending {
   color: Crimson;
   font-weight: bold;
  font-size: 15px;
    margin-bottom: 25px;
}
done {
  color: lightgreen;
   font-weight: bold;
  font-size: 15px;
    margin-bottom: 25px;
  
}
new {
   color: HotPink;
   font-weight: bold;
  font-size: 15px;
line-height:40px;
}
</style>
